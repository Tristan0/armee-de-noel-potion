# Armée de Noël - Potion

Ce petit script Python est un outils pour les MJ du JDR l'_armée de Noël_.
Il permet de valider ou non une préparation proposée par les PJ selon une recette décrite dans le scénario.

## Utilisation
Pour utiliser ce script, il suffit de lancer le fichier `potion.py` avec Python (testé sur python 3.10).

Il faut alors rentrer la liste d'ingrédients en utilisant leurs diminutifs (listés ci-dessous).
Il faut rentrer tous les ingrédients, y compris l'action de mélanger, et appuyer sur 'Entrée' après chacun d'entre eux.
Il est également possible de copier coller la liste des ingrédients séparés par un retour à la ligne depuis un éditeur de texte.

Une fois tous les ingrédients rentrés, il suffit d'appuyer sur 'Entrée' sans mettre d'ingrédient, et la validation est lancée.
Elle affiche :
* la liste d'ingrédients donnée
* les règles qui ne sont pas respectées (s'il y en a)
* les ingrédients qui manquent vis-à-vis de la recette du scénario

## Diminutifs des ingrédients

* eau : **e**
* action de mélanger : **m**
* jus de citron : **jdc**
* sciure de bois : **sc**
* farine : **f**
* baie de genièvre : **b**
* feuille de blette : **fdb**
* poil de belette : **pdb**
* bave de limace : **bdl**
* houx : **h**
* chapon rôti : **c**
* marrons chauds : **mr**

## Exemples

Exemple de préparation invalide :
```
c
c
jdc
m
e
m
e
m
h
mr
e
m
```

Exemple de préparation valide et respectant la recette :
```
c
bdl
m
e
m
f
jdc
m
f
e
m
jdc
m
sc
e
m
f
e
m
f
mr
f
e
m
sc
e
m
h
f
e
m
h
fdb
f
e
m
f
pdb
f
e
m
b
mr
```